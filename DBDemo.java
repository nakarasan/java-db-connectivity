package singleton_Pattern;

import java.sql.SQLException;
import java.util.Scanner;

public class DBDemo {

	public static void main(String[] args) {
		JDBCSingleton jdbc = JDBCSingleton.getInstance();
		//System.out.println("Data inserted");
		try {
			//jdbc.insertData("3", "Rajee");
			//jdbc.readData();
			//jdbc.readData("");
			//jdbc.updateData("Kara", 25);
			jdbc.readData();
			//jdbc.insertData("Shaam",21);
			//System.out.println("Data inserted");
			jdbc.deleteData("Sumi");
			jdbc.readData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
