package singleton_Pattern;

import java.sql.SQLException;

public class DBManipulationDemo {

	public static void main(String[] args) {
		DBQuery dbQuery = DBQuery.getInstance();
		try {
			dbQuery.insertData("OOP", 40);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
